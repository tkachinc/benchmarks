## Autoloader
Recommended autoloader
A more simple and fast autoloader than the one that goes to the composer.

- Connection time autoloader this file is ~ 0.00060 (0.00322 ~ complete initialization)
- Composer autoloader ~ 0.008054 (0.01221 ~ complete initialization)

#### Setting Example
It should be at the project to create a file autoload.php (an example can be taken from the example\autoload\autoload.php)
This file contains two arrays, the first array - the connection of individual classes of non-standard directory.
The second array - connect all libraries namespace (in support for PSR-0 and PSR-4)

#### Example

```php
<?php
require_once(__DIR__."/SimpleAutoload.php");
$autoinclude = [
	VENDOR_PATH.'/react/promise/src/functions_include.php',
];
$classMap = [
	'LightOpenID' => VENDOR_PATH.'/lightopenid/lightopenid/openid.php',
];
$fallbackDirs = [
	'Evenement'                             => VENDOR_PATH.'/evenement/evenement/src',
	'Symfony'                               => VENDOR_PATH.'/gollariel/km-core/src',
	'Symfony\\Component\\HttpFoundation\\'  => VENDOR_PATH.'/symfony/http-foundation',
	'Symfony\\Component\\Filesystem\\'      => VENDOR_PATH.'/symfony/filesystem',
	'Symfony\\Component\\EventDispatcher\\' => VENDOR_PATH.'/symfony/event-dispatcher',
	'Twig'                                  => VENDOR_PATH.'/twig/twig/lib',
	//'Ratchet'                               => VENDOR_PATH.'/cboden/ratchet/src',
	'React\\Stream\\'                       => VENDOR_PATH.'/react/stream/src',
	'React\\Socket\\'                       => VENDOR_PATH.'/react/socket/src',
	'React\\SocketClient\\'                 => VENDOR_PATH.'/react/socket-client/src',
	'React\\Promise\\'                      => VENDOR_PATH.'/react/promise/src',
	'React\\Http\\'                         => VENDOR_PATH.'/react/http/src',
	'React\\HttpClient\\'                   => VENDOR_PATH.'/react/http-client/src',
	'React\\EventLoop\\'                    => VENDOR_PATH.'/react/event-loop',
	'React\\Dns\\'                          => VENDOR_PATH.'/react/dns',
	'React\\ChildProcess\\'                 => VENDOR_PATH.'/react/child-process',
	'React\\Cache\\'                        => VENDOR_PATH.'/react/cache',
	'React\\ZMQ'                            => VENDOR_PATH.'/react/zmq/src',
	'Ratchet\\'                             => VENDOR_PATH.'/cboden/ratchet/src/Ratchet',
	'Guzzle\\Stream'                        => VENDOR_PATH.'/guzzle/stream',
	'Guzzle\\Parser'                        => VENDOR_PATH.'/guzzle/parser',
	'Guzzle\\Http'                          => VENDOR_PATH.'/guzzle/http',
	'Guzzle\\Common'                        => VENDOR_PATH.'/guzzle/common',
];
foreach ($autoinclude as $item)
{
	if(file_exists($item))
	{
		include $item;
	}
}
$autoloader = new SimpleAutoload($classMap, $fallbackDirs);
spl_autoload_register([$autoloader, 'loadClass']);
```

** Файл scaner.php - позволяет быстро получить массив подключаемых классов **
