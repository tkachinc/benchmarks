<?php

/**
 * Class SimpleAutoload
 */
class SimpleAutoload
{
	protected $classMap, $ext, $fallbackDirs;

	/**
	 * @param array $classMap
	 * @param array $fallbackDirs
	 */
	public function __construct(Array $classMap = [], Array $fallbackDirs = [])
	{
		$this->classMap = $classMap;
		$this->fallbackDirs = $fallbackDirs;
	}

	/**
	 * @param $class
	 * @return bool
	 */
	public function loadClass($class)
	{
		if ($file = $this->findFile($class)) {
			$this->includeFile($file);

			return true;
		}

		return false;
	}

	/**
	 * @param array $classArray
	 * @return null|string
	 */
	public function checkFallbackDirs(Array $classArray)
	{
		$ident = reset($classArray);
		$path = implode(DIRECTORY_SEPARATOR, $classArray);
		if (isset($this->fallbackDirs[$ident]) &&
			file_exists($file = $this->fallbackDirs[$ident] . DIRECTORY_SEPARATOR . $path . '.php')
		) {
			return $file;
		}

		$ident = '';
		foreach ($classArray as $item) {
			$ident .= $item;
			$file = $this->checkPath($ident, $classArray, $path);
			if ($file) {
				return $file;
			}
			$file = $this->checkPath($ident . '\\', $classArray, $path);
			if ($file) {
				return $file;
			}
			$ident .= '\\';
		}

		return null;
	}

	/**
	 * @param $ident
	 * @param $classArray
	 * @param $path
	 * @return string
	 */
	protected function checkPath($ident, $classArray, $path)
	{
		if (isset($this->fallbackDirs[$ident])) {
			$namespaceArray = explode('\\', $ident);
			$diff = array_diff_assoc($classArray, $namespaceArray);
			$path2 = implode(DIRECTORY_SEPARATOR, $diff);

			$file = $this->fallbackDirs[$ident] . DIRECTORY_SEPARATOR . $path2 . '.php';

			if (file_exists($file)) {
				return $file;
			}

			$file = $this->fallbackDirs[$ident] . DIRECTORY_SEPARATOR . $path . '.php';

			if (file_exists($file)) {
				return $file;
			}
		}

		return null;
	}

	/**
	 * @param $class
	 * @return null|string
	 */
	protected function findFile($class)
	{
		if (isset($this->classMap[$class]) && file_exists($this->classMap[$class])) {
			return $this->classMap[$class];
		}
		$classArray = explode('\\', $class);
		// PSR-4/1
		$result = $this->checkFallbackDirs($classArray);
		if ($result) {
			return $result;
		}

		if (strpos($class, '_') !== false) {
			// PSR-0 deprecated
			$classArray = explode('_', $class);
			$result = $this->checkFallbackDirs($classArray);
			if ($result) {
				return $result;
			}
		}

		return null;
	}

	/**
	 * @param $file
	 */
	protected function includeFile($file)
	{
		include $file;
	}
}