<?php
define('REAL_PATH', realpath(__DIR__ . '/../'));
define('VENDOR_PATH', realpath(__DIR__ . '/../vendor'));

$rd = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('../'));
$autoIncludeFiles = '$autoinclude = [' . PHP_EOL;
$classMapStr = '$classMap = [' . PHP_EOL;
$fallbackDirsStr = '$fallbackDirs = [' . PHP_EOL;
/** @var SplFileInfo $file */
foreach ($rd as $file) {
	if ($file->getFilename() === 'composer.json' && strpos($file->getPath(), 'example') == false) {
		$patch = $file->getPath();
		if (strpos($file->getPath(), '../vendor') !== false) {
			$patch = str_replace('../vendor', '', $patch);
			$constName = 'VENDOR_PATH';
			//echo $patch.'/'.PHP_EOL;
		} elseif ($file->getPath() === '..') {
			//echo '/'.PHP_EOL;
			$patch = str_replace('..', '', $patch);
			$fileObject = $file->openFile('r');
			$constName = 'REAL_PATH';
		} else {
			continue;
		}

		$json = file_get_contents($file->getPath() . '/' . $file->getFilename(), 'r');
		$data = json_decode($json, true);
		if (isset($data['autoload'])) {
			if (isset($data['autoload']['psr-0'])) {
				foreach ($data['autoload']['psr-0'] as $namespace => $dir) {
					$namespace = str_replace('_', '', $namespace);
					$namespace = trim($namespace, '\\');
					$res = trim(trim($patch, '/') . "/" . trim($dir, '/'), '/');
					$fallbackDirsStr .= "   '" . $namespace . "' => realpath(" . $constName . ".'/" . rtrim($res, '\\') . "')," . PHP_EOL;
				}
			}
			if (isset($data['autoload']['psr-4'])) {
				foreach ($data['autoload']['psr-4'] as $namespace => $dir) {
					$namespace = str_replace('_', '', $namespace);
					$namespace = trim($namespace, '\\');
					$res = trim(trim($patch, '/') . "/" . trim($dir, '/'), '/');
					$fallbackDirsStr .= "   '" . $namespace . "' => realpath(" . $constName . ".'/" . rtrim($res, '\\') . "')," . PHP_EOL;
				}
			}
			if (isset($data['autoload']['classmap'])) {
				foreach ($data['autoload']['classmap'] as $name) {
					foreach (new RecursiveIteratorIterator(
						         new RecursiveDirectoryIterator(VENDOR_PATH . '/' . $patch . '/' . $name)
					         ) as $item) {
						$content = file_get_contents($item);
						$content = preg_replace('#/\*[^*]*\*+([^/][^*]*\*+)*/#', '', $content);
						//$content = preg_replace('!/\*.*?\*/!s', '', $content);
						//$content = preg_replace('/\n\s*\n/', "\n", $content);
						preg_match_all("/[^a-zA-Z ]+(trait|interface|class) (\w+)/", $content, $output_array);
						if (isset($output_array[2])) {
							foreach ($output_array[2] as $class) {
								if ($class === 'defined') {
									echo $content;
									exit;
								}
								$classMapStr .= "   '" . $class . "' => " . $constName . ".'" . $patch . "/" . $name . "'," . PHP_EOL;
							}
						}
					}
				}
			}
			if (isset($data['autoload']['files'])) {
				foreach ($data['autoload']['files'] as $name) {
					$autoIncludeFiles .= "   " . $constName . ".'" . $patch . '/' . $name . "'," . PHP_EOL;
				}
			}
		}
	}
}
$fallbackDirsStr .= '];' . PHP_EOL;
$classMapStr .= '];' . PHP_EOL;
$autoIncludeFiles .= '];' . PHP_EOL;

echo '<?php';
echo PHP_EOL;
echo 'require_once(VENDOR_PATH."/tkachinc/core/autoload/SimpleAutoload.php");';
echo PHP_EOL;
echo $autoIncludeFiles;
echo PHP_EOL;
echo $classMapStr;
echo PHP_EOL;
echo $fallbackDirsStr;
echo PHP_EOL;
echo 'foreach ($autoinclude as $item)' . PHP_EOL;
echo '{' . PHP_EOL;
echo '  if(file_exists($item))' . PHP_EOL;
echo '  {' . PHP_EOL;
echo '      include $item;' . PHP_EOL;
echo '  }' . PHP_EOL;
echo '}' . PHP_EOL;
echo PHP_EOL;
echo '$autoloader = new SimpleAutoload($classMap, $fallbackDirs);';
echo PHP_EOL;
echo 'spl_autoload_register([$autoloader, \'loadClass\']);';