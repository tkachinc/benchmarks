<?php
/**
 * Created by PhpStorm.
 * User: gollariel
 * Date: 11/8/16
 * Time: 10:34
 */

namespace TkachInc\Benchmarks\Storage;

interface StorageInterface
{
	public function save(Array $result);

	public function result():\Generator;
}