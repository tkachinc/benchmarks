<?php
namespace TkachInc\Benchmarks\Storage;

/**
 * Created by PhpStorm.
 * User: gollariel
 * Date: 11/8/16
 * Time: 10:30
 */
class File implements StorageInterface
{
	protected $basePath;
	protected $logKey;

	public function __construct($basePath = null, $logKey = null)
	{
		if (!file_exists($basePath)) {
			if (!defined('REAL_PATH')) {
				throw new \Exception('Not found $baseName and not fount REAL_PATH');
			}
			$this->basePath = REAL_PATH;
		} else {
			$this->basePath = $basePath;
		}

		$this->basePath = realpath($this->basePath);

		if (!file_exists($this->basePath . '/benchmarks')) {
			mkdir($this->basePath . '/benchmarks', 0777);
			mkdir($this->basePath . '/benchmarks/logs', 0777);
			mkdir($this->basePath . '/benchmarks/scripts', 0777);
		}

		if (!$logKey) {
			$this->logKey = dechex(time()) . sha1(uniqid() . getmypid()) . dechex(time());
		} else {
			$this->logKey = $logKey;
		}
	}

	/**
	 * @param array $result
	 */
	public function save(Array $result)
	{
		if (!empty($result)) {
			$file = new \SplFileObject($this->basePath . '/benchmarks/logs/' . $this->logKey . '.out', 'a');
			if ($file) {
				$str = implode(PHP_EOL, $result);
				$file->fwrite($str . PHP_EOL);
			}
			unset($file);
		}
	}

	/**
	 * @return \Generator
	 * @throws \Exception
	 */
	public function result():\Generator
	{
		$file = new \SplFileObject($this->basePath . '/benchmarks/logs/' . $this->logKey . '.out', 'r');
		$file->rewind();
		while (true) {
			try {
				$buffer = $file->fgets();
			} catch (\Exception $e) {
				continue;
			}
			if ($buffer === false || $buffer === '' || $buffer === null) {
				break;
			}
			yield $buffer;
		}
		if (!$file->eof()) {
			throw new \Exception('Error fgets');
		}
	}
}