<?php
namespace TkachInc\Benchmarks;

use TkachInc\Benchmarks\Storage\StorageInterface;
use TkachInc\ProgressBar\Progress\ProgressBar;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Benchmark
{
	protected $start;
	protected $end;
	protected $count = 1;
	protected $usleep = 0;
	protected $percent = 100;
	protected $function;
	protected $afterCallback;
	protected $beforeCallback;
	protected $storage;
	protected $freqSave = 1;
	protected $currentSize = 0;
	protected $toSave = [];

	/**
	 * @param StorageInterface $storage
	 * @param \Closure $function
	 * @param int $count
	 * @param int $freq
	 * @param int $usleep
	 * @param callable|null $afterCallback
	 * @param callable|null $beforeCallback
	 * @return CalculationResult
	 */
	public static function test(StorageInterface $storage,
	                            \Closure $function,
	                            $count = 1,
	                            $freq = 10,
	                            $usleep = 0,
	                            Callable $afterCallback = null,
	                            Callable $beforeCallback = null)
	{
		if ($freq > $count) {
			$freq = $count;
		}

		$bench = new Benchmark($storage,
			$function,
			$count,
			$usleep,
			$afterCallback,
			$beforeCallback);
		$bench->start();
		$gen = $bench->run();

		ProgressBar::displayDefaultBar($gen,
			function () {
			},
			$count,
			0,
			$freq);

		$bench->save();

		return new CalculationResult($storage);
	}

	/**
	 * Benchmark constructor.
	 * @param StorageInterface $storage
	 * @param \Closure $function
	 * @param int $count
	 * @param int $usleep
	 * @param callable|null $afterCallback
	 * @param callable|null $beforeCallback
	 * @throws \Exception
	 */
	public function __construct(StorageInterface $storage,
	                            \Closure $function,
	                            $count = 1,
	                            $usleep = 0,
	                            Callable $afterCallback = null,
	                            Callable $beforeCallback = null)
	{
		if ($count <= 0) {
			$count = 1;
		}
		$freqSave = $this->getFreqSave($count);
		if ($freqSave > $count) {
			$freqSave = $count;
		}

		$this->freqSave = $freqSave;
		$this->usleep = $usleep;
		$this->afterCallback = $afterCallback;
		$this->beforeCallback = $beforeCallback;
		$this->count = $count;
		$this->function = $function;
		$this->storage = $storage;
	}

	/**
	 * @param $count
	 * @return float|int
	 */
	public function getFreqSave($count)
	{
		$freqSave = (int)$count / 10;

		return $freqSave;
	}

	/**
	 * @return \Generator
	 * @throws \Exception
	 */
	public function run()
	{
		$count = $this->count;
		$itr = 0;
		$startTime = microtime(true);
		while ($this->count > 0) {
			if (is_callable($this->beforeCallback)) {
				call_user_func_array($this->beforeCallback, []);
			}

			ob_start();
			$this->start();
			call_user_func_array($this->getFunction(), []);
			$this->end();
			$this->count--;
			$this->save();
			ob_end_clean();

			if (is_callable($this->afterCallback)) {
				call_user_func_array($this->afterCallback, []);
			}

			if ($this->usleep > 0) {
				usleep($this->usleep);
			}

			$itr++;

			$secondsLeft = microtime(true) - $startTime;
			$secondsToEnd = ($secondsLeft * $count / $itr) - $secondsLeft;
			$timestamp = time() + $secondsToEnd;
			$date = new \DateTime();
			$date->setTimestamp($timestamp);

			$percent = ($itr * $this->percent) / $count;
			$speed = max(round($itr / $secondsLeft, 2), 0);

			yield new GenerationResult($secondsToEnd,
				$secondsLeft,
				$date->format('Y-m-d H:i:s'),
				date('Y-m-d H:i:s'),
				$percent,
				$speed,
				$this->count,
				$itr,
				round($this->percent - $percent, 2));
		}
		$this->saveCache();
	}

	public function getStorage()
	{
		return $this->storage;
	}

	public function calc()
	{
		return new CalculationResult($this->storage);
	}

	/**
	 * @throws \Exception
	 */
	public function save()
	{
		if (!isset($this->start)) {
			throw new \Exception('Start is required');
		}

		if (!isset($this->end)) {
			$this->end();
		}

		$this->currentSize++;

		$this->toSave[] = abs($this->end - $this->start);
		if ($this->freqSave >= $this->currentSize) {
			$this->storage->save($this->toSave);
			$this->currentSize = 0;
			$this->toSave = [];
		}
	}

	public function saveCache()
	{
		if ($this->currentSize > 0) {
			$this->storage->save($this->toSave);
			$this->currentSize = 0;
			$this->toSave = [];
		}
	}

	public function start()
	{
		$this->start = microtime(true);
	}

	public function end()
	{
		$this->end = microtime(true);
	}

	/**
	 * @return \Closure
	 */
	public function getFunction()
	{
		return $this->function;
	}
}