<?php
/**
 * Created by PhpStorm.
 * User: gollariel
 * Date: 11/11/16
 * Time: 02:54
 */

namespace TkachInc\Benchmarks;

use TkachInc\Benchmarks\Storage\StorageInterface;

class CalculationResult
{
	protected $first = 0, $min = 0, $max = 0, $avg = 0;

	/**
	 * CalculationResult constructor.
	 * @param StorageInterface $storage
	 */
	public function __construct(StorageInterface $storage)
	{
		$count = 0;
		$sum = 0;
		$first = null;
		$min = null;
		$max = 0;

		$generator = $storage->result();
		foreach ($generator as $buffer) {
			$time = (float)$buffer;
			if ($first === null) {
				$first = $time;
			}
			if ($min === null || $time < $min) {
				$min = $time;
			}
			if ($buffer > $max) {
				$max = $time;
			}

			$sum += (float)$time;
			$count++;
		}

		$this->first = $first;
		$this->min = $min;
		$this->max = $max;
		$this->avg = ($sum / $count);
	}

	/**
	 * @return float
	 */
	public function getAverage()
	{
		return $this->avg;
	}

	/**
	 * @return float
	 */
	public function getMin()
	{
		return $this->min;
	}

	/**
	 * @return float
	 */
	public function getMax()
	{
		return $this->max;
	}

	/**
	 * @return float
	 */
	public function getFirst()
	{
		return $this->first;
	}
}