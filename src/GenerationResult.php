<?php
/**
 * Created by PhpStorm.
 * User: gollariel
 * Date: 11/11/16
 * Time: 02:54
 */

namespace TkachInc\Benchmarks;

class GenerationResult
{
	protected $secondsToEnd, $secondsLeft, $datetimeEnd, $currentDatetime, $percent, $speed, $count, $iteration, $percentToEnd;

	/**
	 * GenerationResult constructor.
	 * @param $secondsToEnd
	 * @param $secondsLeft
	 * @param $datetimeEnd
	 * @param $currentDatetime
	 * @param $percent
	 * @param $speed
	 * @param $count
	 * @param $iteration
	 * @param $percentToEnd
	 */
	public function __construct($secondsToEnd,
	                            $secondsLeft,
	                            $datetimeEnd,
	                            $currentDatetime,
	                            $percent,
	                            $speed,
	                            $count,
	                            $iteration,
	                            $percentToEnd)
	{
		$this->secondsToEnd = $secondsToEnd;
		$this->secondsLeft = $secondsLeft;
		$this->datetimeEnd = $datetimeEnd;
		$this->currentDatetime = $currentDatetime;
		$this->percentToEnd = $percent;
		$this->speed = $speed;
		$this->count = $count;
		$this->iteration = $iteration;
		$this->percentToEnd = $percentToEnd;
	}

	/**
	 * @return mixed
	 */
	public function getSecondsLeft()
	{
		return $this->secondsLeft;
	}

	/**
	 * @return mixed
	 */
	public function getSecondsToEnd()
	{
		return $this->secondsToEnd;
	}

	/**
	 * @return mixed
	 */
	public function getDatetimeEnd()
	{
		return $this->datetimeEnd;
	}

	/**
	 * @return mixed
	 */
	public function getPercent()
	{
		return $this->percent;
	}

	/**
	 * @return mixed
	 */
	public function getSpeed()
	{
		return $this->speed;
	}

	/**
	 * @return mixed
	 */
	public function getCount()
	{
		return $this->count;
	}

	/**
	 * @return mixed
	 */
	public function getIteration()
	{
		return $this->iteration;
	}

	/**
	 * @return mixed
	 */
	public function getPercentToEnd()
	{
		return $this->percentToEnd;
	}

	/**
	 * @return mixed
	 */
	public function getCurrentDatetime()
	{
		return $this->currentDatetime;
	}
}