<?php
/**
 * @author maxim
 */
define('APP_PATH', 'app');
define('REAL_PATH', '.');
define('CONFIG_PATH', 'app/config');
define('VENDOR_PATH', 'vendor');

/**
 * Подключение автолоадера
 */
require_once(REAL_PATH . "/autoload/SimpleAutoload.php");
$fallbackDirs = [
	'TkachInc\Benchmarks'  => realpath(REAL_PATH . '/src'),
	'TkachInc\ProgressBar' => realpath(VENDOR_PATH . '/tkachinc/progress-bar/src'),
	'TkachInc\CLI'         => realpath(VENDOR_PATH . '/tkachinc/cli/src'),
];
$autoload = new SimpleAutoload([], $fallbackDirs);
spl_autoload_register([$autoload, 'loadClass']);


/**
 * @param int - count repeat function
 * @param function|string - test
 * @param string - output file
 */
$data = range(0, 10000);
$calc = \TkachInc\Benchmarks\Benchmark::test(new \TkachInc\Benchmarks\Storage\File(), function()use($data){
	//TEST_1
	json_encode($data);
}, 100000);
echo PHP_EOL;
echo 'Average: '.$calc->getAverage();
echo PHP_EOL;