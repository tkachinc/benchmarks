[![Codacy Badge](https://api.codacy.com/project/badge/Grade/475b1b96c8d94d8aa4bfc2fc4ea90257)](https://www.codacy.com/app/gollariel/benchmarks?utm_source=tkachinc@bitbucket.org&amp;utm_medium=referral&amp;utm_content=tkachinc/benchmarks&amp;utm_campaign=Badge_Grade)

### Universal benchmark your PHP code

## Install

You need add "tkachinc/benchmarks":">=2" to you composer.json, or run command:

```sh
php composer.phar require tkachinc/benchmarks:>=2
```

You can find example in benchmark.php